<?php

function palindrome_angka($angka) {
    if($angka == strrev($angka)){
        $angka = $angka+1;
        return $angka."<br>";
    }else{
        $test = true;
        while($test){
            $angka = $angka+1;
            if($angka == strrev($angka)){
                $test = false;
                return $angka."<br>";
            }
        }
    } 
}

// TEST CASES
echo palindrome_angka(8); // 9
echo palindrome_angka(10); // 11
echo palindrome_angka(117); // 121
echo palindrome_angka(175); // 181
echo palindrome_angka(1000); // 1001



?> 