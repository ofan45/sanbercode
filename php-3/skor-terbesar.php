<?php
function skor_terbesar($arr){
//kode di sini
    echo '<pre>';
    $arr_data = [];
    foreach ($arr as $cel) {
        if(array_key_exists($cel['kelas'], $arr_data)){
            if($cel['nilai']> $arr_data[$cel['kelas']]['nilai']){
                $arr_data[$cel['kelas']] = $cel;
            }
        }else{
            $arr_data[$cel['kelas']] = $cel;
        }
    }

    print_r($arr_data);
    echo '<pre>';
}

// TEST CASES
$skor = [
  [
    "nama" => "Bobby",
    "kelas" => "Laravel",
    "nilai" => 78
  ],
  [
    "nama" => "Regi",
    "kelas" => "React Native",
    "nilai" => 86
  ],
  [
    "nama" => "Aghnat",
    "kelas" => "Laravel",
    "nilai" => 90
  ],
  [
    "nama" => "Indra",
    "kelas" => "React JS",
    "nilai" => 85
  ],
  [
    "nama" => "Yoga",
    "kelas" => "React Native",
    "nilai" => 77
  ],
];

print_r(skor_terbesar($skor));
/* OUTPUT
  Array (
    [Laravel] => Array
              (
                [nama] => Aghnat
                [kelas] => Laravel
                [nilai] => 90
              )
    [React Native] => Array
                  (
                    [nama] => Regi
                    [kelas] => React Native
                    [nilai] => 86
                  )
    [React JS] => Array
                (
                  [nama] => Indra
                  [kelas] => React JS
                  [nilai] => 85
                )
  )
*/
?>