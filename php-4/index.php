<?php
  require "animal.php";
  $sheep = new Animal("shaun");
  echo $sheep->name."<br>"; // "shaun"
  echo $sheep->legs."<br>"; // 2
  echo $sheep->cold_blooded."<br>"; // false

  
  $sungokong = new Ape("kera sakti");
  echo $sungokong->yell();
    
  $kodok = new Frog("buduk");
  echo $kodok->jump();

?>